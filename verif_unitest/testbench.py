import sys
sys.path
sys.path.append('../sim/')
from riscv_alu import riscv_alu
from riscv_alu import ALUOp
from riscv_alu import ALUPortIO
import random
from myhdl import *
import unittest

class UnitTests(unittest.TestCase):

    def testSum(self):
        """ Testeo simple para probar el flujo de test unitarios """

        def test(aluIO, clk, rst):
            aluIO.alu_a_i.next = random.randrange(0, 2**32)
            aluIO.alu_b_i.next = random.randrange(0, 2**32)
            aluIO.alu_op_i.next = ALUOp.ALU_ADD
            yield delay(2)
            self.assertEqual(aluIO.alu_p_o,modbv(aluIO.alu_a_i + aluIO.alu_b_i)[32:])

        self.runTests(test)

    def testXor(self):
        """ Testeo simple para probar el flujo de test unitarios """

        def test(aluIO, clk, rst):
            aluIO.alu_a_i.next = random.randrange(0, 2**32)
            aluIO.alu_b_i.next = random.randrange(0, 2**32)
            aluIO.alu_op_i.next = ALUOp.ALU_XOR
            yield delay(2)
            self.assertEqual(aluIO.alu_p_o,aluIO.alu_a_i ^ aluIO.alu_b_i)

        self.runTests(test)


    def runTests(self, test):
        """Funcion para correr los tests, aca se pueden cambiar parametros de la dut
           Por ejemplo cambiar el numero de bits de la ALU y correr los unitests para 
           diferentes parametros. """
        clk = Signal(False)
        rst = Signal(True)
        aluIO = ALUPortIO()
        dut = riscv_alu(io_iface=aluIO)
        dut = toVerilog(dut,aluIO)
        check = test(aluIO,clk, rst)
        sim = Simulation(dut, check)
        sim.run(quiet=1)
            
if __name__ == "__main__":
    unittest.main(verbosity=2)
